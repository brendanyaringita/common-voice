## General

yes-receive-emails = እወ ኢ-መይል ስደደለይ። ብዛዕባ ፕሮጀክት ሓባራዊ ድምጺ ኽሕብር እደሊ እየ።
stayintouch = ኣብ ሞዚላ ኣብ ከባቢ ተክኖሎጂ ድምጺ ማሕበረሰብ ንሃንጽ ኣሎና ። ምስ ምትዕርራያት ንሓድሽ ምንጭታት ሓበሬታ ከምኡውን ነዚ ሓበሬታ እዚ ብኸመይ ከም እትጥቀመሉ ዝያዳ ኽንሰምዕ ንደሊ ኢና።
privacy-info = ሓበሬታኻ ብጥንቃቐ ኸም እንሕዞ ንመባጻዕ ። ኣብታ <privacyLink>ናይ ምስጢር ምልክታ</privacyLink> ዘሕለፋ ዝያዳ ኣንብብ።
return-to-cv = ናብ ሓባራዊ ድምጺ ተመለስ
email-input =
    .label = ኢ-መይል
submit-form-action = ኣቅርብ
loading = ይጽዓን...
email-opt-in-info = ከም ናይ ሸቶ መዘኻኸሪታት ብዛዕባ ዕቤት ዝገብሮ ምትዕርራያት ከምኡውን ብዛዕባ ሓባራዊ ድምጺ ዚገልጽ ጋዜጣታት ዝኣመሰለ ኢ-መይል ክቕበል እደሊ እየ።
email-opt-in-info-title = ምስ ዝርዝር ደብዳበታት ሓባራዊ ድምጺ ተጸምበር
email-opt-in-info-sub-with-challenge = ከም ብድሆታትን ሸቶታትን ዜዘኻኽር ነገራት እዋናዊ ሓበሬታ ምዕባለ ከምኡውን ብዛዕባ ሓባራዊ ድምጺ ዚገልጽ ጋዜጣታት ዝኣመሰለ ኢ-መይል ተቐበል።
email-opt-in-privacy-v2 = ኢ-መይል ክትቅበል ብምምራጽካ ሞዚላ ነዚ ሓበሬታ እዚ ክትሕዞ ኣብ ናይ ሞዚላ <privacyLink>ናይ ምስጢር ፖሊሲ</privacyLink> ከምዝገለጸቶ ትሰማማዕ ኢኻ።
indicates-required = ኣድላዪ ቦታ ይሕብር
not-available-abbreviation = ይሎን

# Don't rename the following section, its contents are auto-inserted based on the name (see scripts/pontoon-languages-to-ftl.js)
# [Languages]


## Languages

ab = ኣብኻዝያ
ace = ኣሰሀነስ
ady = ኣድይገ
af = ኣፍሪካስ
am = ኣምሓርኛ
an = ኣራጎነስ
ar = ዓረብ
arn = ማፓዱጉነ
as = ኣሳመሰ
ast = ኣሰቱርያን

# [/]


## Layout


## Home Page

wall-of-text-start =
    ድምጺ ባህርያዊ እዩ ድምጺ ኸኣ ሰብ እዩ። ስለዚ ኸኣ ኢና ዝጠቅም ድምጺ ብምፍጣርና እንምሰጥ።
    ንማሽናትና ዚኸውን ተክኖሎጂ። ስርዓተ ድምጺ ንምፍጣር ግና ኣዝዩ ብዙሕ ድምጺ
    ሓበሬታ የድሊ ።
wall-of-text-more-mobile =
    መብዛሕትኡ ዓበይቲ ኩባንያታት ዚጥቀማሉ ሓበሬታ ንመብዛሕትኦም ሰባት ኣይርከብን እዩ። እዚ ኸኣ ሓድሽ ምህዞታት ከም  ዝዓግት ንኣምን። ስለዚ ድምጺ ንምስማዕ ዚሕግዝ ፕሮጀክት ሓባራዊ ድምጺ ፕሮጀክት ኣጀሚርና ኣለና
    ተፈላጥነት ንዅሉ ሰብ ክፉት እዩ ።

## Account Benefits


## What's public


## Speak & Listen Shortcuts


## Listen Shortcuts


## Speak Shortcuts


## ProjectStatus


## ProfileForm


## Profile - Email


## Profile - Email


## FAQ

faq-why-mission-a = ሞዚላ ነቲ መርበብ ሓበሬታ ንዅሉ ሰብ ክፉት ንምግባርን ብቐሊሉ ንምርካብን እተወፈየት እያ ። ነዚ ንምግባር ንኸም ሓባራዊ ድምጺ ዝኣመሰለ ፕሮጀክትታት ኣቢልና ንፈጠርቲ ወብ ሳይት ሓይሊ ኽንህቦም ኣሎና። ተክኖሎጅያዊ መሳርሒታት ድምጺ ኻብ ጽቡቕ መዓላ ወጻኢ እናበዝሐ ብዝኸደ መጠን ንዅሎም ተጠቀምቲ ብማዕረ ኼገልግሉ ኸም ዘለዎም ንኣምን ኢና። እዚ ኸኣ ተክኖሎጅያዊ ድምጺ ኽትሃንጽን ክትፍትንን ከለኻ ዝያዳ ቛንቋታት ምውፋር ከምኡውን እተፈላለየ ኣደማምጻን ቍጽሪ ህዝብን ምርካብ ማለት እዩ። ሓባራዊ ድምጺ ንዅሎም ሰባት ከምኡውን ኣብ መላእ ዓለም ዝርከቡ ጕጅለታት ሞዚላን ኣዳለውትን ድሮ ኣብ ናይ ገዛእ ርእስና ፕሮጀክትታት ክጥቀሙሉ ዝኽእሉ ህዝባዊ ምንጪ ኢዩ።
faq-what-cv-and-deepspeech-q = ኣብ መንጎ ሓባራዊ ድምጽን ዓሚቝ ዘረባን (Deep Speech) እንታይ ፍልልይ ኣሎ?
faq-what-cv-and-deepspeech-a = እቲ ሓባራዊ ናይ ድምጺ ሓበሬታ ነቲ ሞዚላ እተዳለወትሉ ክፉት ምንጪ ናይ ድምጺ መለለዪ ሞተር ዲፕ ስፒች ዝርርብ መመላእታ ኢዩ ። እቲ ናይ መጀመርታ ሕታም ዓሚቝ ዘረባ ኣብ ሕዳር 2017 ወጸ ካብቲ ግዜ እቲ ኣትሒዙ ድማ ብፍልቀት ይቕጽል ኣሎ። ምስ ናይ ሓባር ድምጺ ሓበሬታ ብሓባር እዚ ክፉት ምንጪ ዘለዎ ናይ ድምጺ መለለዪ ተክኖሎጂ ንዅሉ ሰብ ክረኽቦ ከም ዘለዎ ንኣምን ኢና። እዚ ተክኖሎጅያዊ መሳርሒታት እዚ ንተዓዘብቲ ሓድሽ ፍርያትን ኣገልግሎታትን ንኽሃንጹ ከም ዘኽእሎም ተስፋ ንገብር።
faq-is-goal-assistant-q = ሸቶ ሓባራዊ ድምጺ ሓጋዚ ድምጺ ምህናጽ ድዩ ?
faq-is-goal-assistant-a = ዕላማ ናይ ሓባራዊ ድምጺ ሓበሬታ ዝዀነ ይኹን ኣብ ዓለም ዘሎ ሰብ ናይ ምዝራብ ኣፍልጦ ናይ ተዛራባይ ኣፍልጦ ወይ ናይ ድምጺ ሓበሬታ ዘድልዮ ዝዀነ ይኹን ካልእ ዓይነት ኣወዓዕላ ንምህናጽ ኢዩ። ሓጋዚ ድምጺ ሓደ ኻብቲ ነቲ ሓበሬታ ንምስራሕ ክትጥቀመሉ እትኽእል እተፈላለየ ዓይነት መወከሲታት እዩ ።
faq-do-want-native-q = ደቀባት ዘይኰኑ ተዛረብቲ ስለ ዝዀንኩ ኣደማምጻ(accent) እዛረብ እየ፣ ሕጂ እውን እንተ ዀነ ድምጸይ ትደሊዶ?
faq-do-want-native-a = እወ ብፍላይ ድምጽኻ ንደሊ ኢና! ሓደ ኻብቲ ዕላማ ሓባራዊ ድምጺ ኣገልግሎት ኣፍልጦ ድምጺ ንዅሉ ሰብ ብማዕረ ምእንቲ ኺዓዪ ብእተኻእለ መጠን እተፈላለየ ኣደማምጻ ምእካብ እዩ። እዚ ማለት ከኣ ካብ ደቀባት ዘይኰኑ ተዛረብቲ ዝወሃብ ወፈያ ኣዝዩ ኣድላዪ ኢዩ።
faq-why-different-speakers-q = ኣብ ሓደ ቛንቋ ኽንድዚ ዚኣኽሉ እተፈላለዩ ተዛረብቲ ዜድልዩኻ ስለምንታይ እዮም?
faq-why-different-speakers-a =
    መብዛሕትኡ ናይ ምዝራብ ኣብያተ - ጽሕፈት ንገሊኡ ጂኦግራፍያዊ ኣቀማምጣ ብምዝውተር ኢዩ ዝስልጥን እዚ ድማ ኣብ <articleLink>ተባዕትዮን ማእከላይ ደረጃ ዘለዎምን ኣድልዎ ከም ዝህልወካ ዝገብር ኢዩ </articleLink> ። ኣብ ናይ ስልጠና ሓበሬታ ብዙሕ ዘይውከል ኣደማምጻን ላህጃታትን መብዛሕትኡ ግዜ ምስቶም ድሮ ኣብ ትሕቲ ቝጽጽሩ ዘለዉ ጕጅለታት ሰባት እተተሓሓዘ ኢዩ። ብዙሓት ማሽናት ንድምጺ ደቂ ኣንስትዮ ምርዳእ የጸግሞም እዩ።
    ስለዚ ኸኣ ኢና ኣብ ናይ ድምጽና ሓበሬታ እተፈላለየ ዓይነት ሓበሬታ እንደሊ!
faq-why-my-lang-q = ቋንቋይ ክሳዕ ሕጂ ስለምንታይ እዩ  ዘይተጠቓለለ ?
faq-why-my-lang-new-a = ሞዚላ ንሓደ ቛንቋ ኻብ ካልእ ቋንቋ ኣይትመርጾን ወይ ኣይተዳሉን እያ። ኣብ ክንድኡስ ሓባራዊ ድምጺ ብማሕበረሰብ ዝምራሕ ተበግሶ ኢዩ እንተዀነ ግን ሓድሽ ቋንቋ ንምውሳኽን ናይ ድምጺ ወፈያታት ንምእካብን <multilangLink>እተወሰነ ስጕምትታት </multilangLink> ዝወስድ። ቀዳማይ ኣባላት ማሕበረሰብ ነቲ ኣበርክቶ ዝገበረ ተመክሮ ብቛንቋኦም ምእንቲ ኺረኽብዎ ወብ ሳይት ሓባራዊ ድምጺ ኺትርጐም ኣለዎ። ድሕርዚ ሰባት ዓው ኢሎም ምእንቲ ኼንብቡ መሰል ዋና ዘይብሉ ብዙሕ ምሉእ ሓሳባት የድልየና። እዚ ኽልቲኡ ብቕዓታት እዚ ሓንሳእ ምስ ተፈጸመ ሰባት ድምጾም ኪምዝግቡን ንኻልኦት ወፈያታት ኬረጋግጹን ምእንቲ ኺጅምሩ ሓደ ቛንቋ ኣብ ሓባራዊ ድምጺ ይጅመር። ሓድሽ ቋንቋ ኣብ ምጅማር ክትሕግዝ እንተ ደሊኻ ናብታ <sentenceCollectorLink>ንምእካብ እትሕግዘና </sentenceCollectorLink> ንዋት ኣተኩር።
faq-what-quality-q = ክሊፕ ድምጺ ኣብቲ ሓበሬታ ንምጥቃም እንታይ ዓይነት ኣውድዮ እዩ ዜድሊ?

## ABOUT US


## Glossary


## Error pages


## Data


## Datasets Page


## Download Modal


## Contact Modal


## Request Language Modal


## Languages Overview


## Contribution


## Reporting


## Goals


## Dashboard


## Custom Goals


## Profile Delete


## Profile Download


## Landing


## DemoLayout


## Demo Datasets


## Demo Account


## Demo Contribute


## Demo Dashboard


## Validation criteria

