## General

yes-receive-emails = Пара, кучеда мондейне сёрма. Монь ули мялезе содафтомс «Common Voice» проектть кулянзон мархта.
stayintouch = Минь Мозилласа тихтяма кялень технологиянь коряс ломанень пуромкс. Минь мяленьке, штоба тинь содалесть сембе од кулятнень, од лисмопрятнень, а тяфта жа  тяфтама даннайхнень тевс нолямаснон
privacy-info = Макстама вал цебярьста ваномс информациять мельгя. Сяда крхкаста ванк <privacyLink>Кяшф сёрматфксть эса</privacyLink>.
return-to-cv = Меки «Common Voice-нди»
email-input =
    .label = Эл. адрессь
submit-form-action = Кучемс
loading = Таргави...

# Don't rename the following section, its contents are auto-inserted based on the name (see scripts/pontoon-languages-to-ftl.js)
# [Languages]


## Languages

ab = Абхазонь кяль
ace = Ачехонь кяль
ady = Адыгонь кяль
af = Африкансонь кяль

# [/]


## Layout


## Home Page


## Account Benefits


## What's public


## Speak & Listen Shortcuts


## Listen Shortcuts


## Speak Shortcuts


## ProjectStatus


## ProfileForm


## Profile - Email


## Profile - Email


## FAQ


## ABOUT US


## Glossary


## Error pages


## Data


## Datasets Page

size-gigabyte = ГБ
size-megabyte = МБ

## Download Modal


## Contact Modal


## Request Language Modal


## Languages Overview


## Contribution


## Reporting


## Goals


## Dashboard


## Custom Goals


## Profile Delete


## Profile Download


## Landing


## DemoLayout


## Demo Datasets


## Demo Account


## Demo Contribute


## Demo Dashboard


## Validation criteria

